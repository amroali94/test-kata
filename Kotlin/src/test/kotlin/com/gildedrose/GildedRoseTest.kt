package com.gildedrose

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GildedRoseTest {

    @Test
    fun `quality of normal item decreases by 1`() {
        runSingleDayTestOnItem(Item("Standard Item", 10, 10), 9)
    }

    @Test
    fun `quality of normal item decreases by 2 after sell by date`() {
        runSingleDayTestOnItem(Item("Standard Item", 0, 10), 8)
    }

    @Test
    fun `quality of normal item does not go below 0`() {
        runSingleDayTestOnItem(Item("Standard Item", 10, 0), 0)
    }

    @Test
    fun `quality of Aged Brie increases by 1`() {
        runSingleDayTestOnItem(Item("Aged Brie", 10, 10), 11)
    }

    @Test
    fun `quality of Aged Brie increases by 2 after sell by date`() {
        runSingleDayTestOnItem(Item("Aged Brie", 0, 10), 12)
    }

    @Test
    fun `quality of Aged Brie does not go above 50`() {
        runSingleDayTestOnItem(Item("Aged Brie", 10, 50), 50)
    }

    @Test
    fun `quality of Sulfuras does not change`() {
        runSingleDayTestOnItem(Item("Sulfuras, Hand of Ragnaros", 10, 80), 80)
    }

    @Test
    fun `quality of Backstage Passes increases by 1 when sellIn is more than 10`() {
        runSingleDayTestOnItem(Item("Backstage passes to a TAFKAL80ETC concert", 11, 10), 11)
    }

    @Test
    fun `quality of Backstage Passes increases by 2 when sellIn is 10 or less`() {
        runSingleDayTestOnItem(Item("Backstage passes to a TAFKAL80ETC concert", 10, 10), 12)
    }

    @Test
    fun `quality of Backstage Passes increases by 3 when sellIn is 5 or less`() {
        runSingleDayTestOnItem(Item("Backstage passes to a TAFKAL80ETC concert", 5, 10), 13)
    }

    @Test
    fun `quality of Backstage Passes drops to 0 after the concert`() {
        runSingleDayTestOnItem(Item("Backstage passes to a TAFKAL80ETC concert", 0, 10), 0)
    }

    @Test
    fun `quality of Conjured item decreases by 2`() {
        runSingleDayTestOnItem(Item("Conjured Mana Cake", 10, 10), 8)
    }

    @Test
    fun `quality of Conjured item decreases by 4 after sell by date`() {
        runSingleDayTestOnItem(Item("Conjured Mana Cake", 0, 10), 6)
    }

    private fun runSingleDayTestOnItem(item: Item, expectedQuality: Int) {
        val items = listOf(item)
        val app = GildedRose(items)
        app.updateQuality()
        assertEquals(expectedQuality, app.items[0].quality)
    }

}


