package com.gildedrose

enum class ItemType {
    SULFURAS, AGED_BRIE, BACKSTAGE_PASSES, CONJURED, NORMAL;

    companion object {
        fun fromName(name: String): ItemType {
            return when {
                name.contains("Sulfuras") -> SULFURAS
                name.contains("Aged Brie") -> AGED_BRIE
                name.contains("Backstage passes") -> BACKSTAGE_PASSES
                name.contains("Conjured") -> CONJURED
                else -> NORMAL
            }
        }
    }
}