package com.gildedrose

/**
 * Determine the item type based on its name.
 * This will be recomputed every time the type is accessed, sicne we can't modify the Item class as per requirements
 * it will have to do.
 */
val Item.type: ItemType
    get() = ItemType.fromName(name)

/**
 * Compute the quality change amount for the item based on its type and sellIn value.
 */
fun Item.computeQualityChange(): Int {
    return when (type) {
        // Sulfuras quality never changes
        ItemType.SULFURAS -> 0
        // Aged brie quality increases with time
        ItemType.AGED_BRIE -> if (sellIn > 0) 1 else 2
        // Backstage passes quality increases as the concert date approaches
        ItemType.BACKSTAGE_PASSES -> when {
            sellIn <= 0 -> -quality
            sellIn <= 5 -> 3
            sellIn <= 10 -> 2
            else -> 1
        }
        // Conjured item quality decreases twice as fast
        ItemType.CONJURED -> if (sellIn > 0) -2 else -4
        // Normal item
        ItemType.NORMAL -> if (sellIn > 0) -1 else -2
    }
}

/**
 * Update the quality property of the item based on its type and sellIn value while ensuring the quality is within bounds.
 */
fun Item.updateQuality() {
    // Update quuality based on computed change
    quality += computeQualityChange()

    // Quality cannot be negative
    if (quality < 0) {
        quality = 0
    }

    // Quality cannot be more than 50 except for Sulfuras
    if (quality > 50 && type != ItemType.SULFURAS) {
        quality = 50
    }
}